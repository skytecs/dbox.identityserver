﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.


using IdentityModel;
using IdentityServer4.Models;
using System.Collections.Generic;
using static DBox.IdentityServer.Validation.EmailTokenGrantValidator;

namespace DBox.IdentityServer
{
    public static class Config
    {
        public static IEnumerable<ApiScope> ApiScopes =>
            new ApiScope[]
            {
                new ApiScope("admin"),
                new ApiScope("accounts"),
                new ApiScope("introspection")
            };

        public static IEnumerable<ApiResource> ApiResources => new ApiResource[] {
            new ApiResource("DBoxAdminApi", "DBox Admin API"){
                Scopes = { "admin", "offline_access" },
                UserClaims = {  JwtClaimTypes.Subject, JwtClaimTypes.Email, "name" }
            },
            new ApiResource("DBoxAccountsService", "DBox Accounts Service"){
                Scopes = { "accounts"}
            },
            new ApiResource("DboxIntrospectionService", "Dbox Introspection Service")
            {
                Scopes = { "introspection"},
                Name = "DboxIntrospectService",
                ApiSecrets = {
                    new Secret("3A2B3459-F1E8-4992-8487-1335A1CCB3AC".Sha512())
                }
            }
        };

        public static IEnumerable<Client> Clients =>
            new Client[]
            {
                // m2m client credentials flow client
                new Client
                {
                    ClientId = "DBoxAdminApi",
                    ClientName = "DBox Admin API",
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPasswordAndClientCredentials,
                    ClientSecrets = { new Secret("511536EF-F270-4058-80CA-1C89C192F69A".Sha256()) },

                    AllowedScopes = { "admin", "offline_access" },
                    AllowOfflineAccess = true
               },
                new Client
                {
                    ClientId = "DBoxAccountsService",
                    ClientName = "DBox Accounts Service",
                    AllowedGrantTypes = GrantTypes.ClientCredentials,
                    ClientSecrets = { new Secret("9b5bb4e6-b7d6-4678-95ce-ded8f0c2a138".Sha256()) },

                    AllowedScopes = { "accounts" }
                    
               },
                new Client
                {
                    ClientId = "DBoxPhoneService",
                    AllowedGrantTypes = new List<string> { GrantTypeConst.EmailToken },
                    ClientSecrets = {new Secret("8BA17548-63EB-4E56-B4E2-D7C07777B1D0".Sha256())},
                    AllowedScopes = { "admin", "offline_access", "introspection" },
                    AllowOfflineAccess = true
                },
                new Client
                {
                    ClientId = "DboxIntrospectService",
                    AllowedGrantTypes = new List<string> { GrantType.ClientCredentials, "token" },
                    ClientSecrets = {new Secret("3A2B3459-F1E8-4992-8487-1335A1CCB3AC".Sha256())},
                    AllowedScopes = { "introspection", "offline_access" },
                    AllowOfflineAccess = true
                }
            };
    }
}