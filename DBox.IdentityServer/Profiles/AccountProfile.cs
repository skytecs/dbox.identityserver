﻿using AutoMapper;

namespace DBox.IdentityServer.Profiles
{
    public class AccountProfile : Profile
    {
        public AccountProfile()
        {
            CreateMap<Controllers.CreateAccountRequest, Services.CreateAccountCommand>();
            CreateMap<Controllers.CreateEmptyAccountRequest, Services.CreateEmptyAccountCommand>();
            CreateMap<Controllers.CreateEmailAccountRequest, Services.CreateUserAccountCommand>();

            CreateMap<Controllers.ForgotPasswordRequest, Services.CreateResetPasswordTokenCommand>();
            CreateMap<Services.CreateResetPasswordTokenResult, Controllers.ForgotPasswordResponse>();

            CreateMap<Controllers.ResetPasswordRequest, Services.ResetPasswordCommand>();
            CreateMap<Controllers.RemoveAccountRequest, Services.RemoveAccountCommand>();
            CreateMap<Controllers.UpdateAccountRequest, Services.UpdateAccountCommand>();

            CreateMap<Models.DBoxUser, Services.AccountInfo>();
            CreateMap<Services.AccountInfo, Controllers.Account>();
        }
    }
}
