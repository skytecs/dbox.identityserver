﻿using Consul;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace DBox.IdentityServer.Extensions
{
    public static class ServiceProviderExtensions
    {
        public static Uri DiscoverServiceUrl(this IServiceProvider provider, string serviceName)
        {
            var client = provider.GetService<IConsulClient>() ?? throw new ArgumentNullException(nameof(IConsulClient));
            var config = provider.GetService<IConfiguration>() ?? throw new ArgumentNullException(nameof(IConfiguration));

            var response = client.Agent.Services().Result.Response[config.GetValue<string>(serviceName)];

            return new Uri($"{response.Address}:{response.Port}");
        }
    }
}
