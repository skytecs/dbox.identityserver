﻿using DBox.IdentityServer.Extensions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;

namespace DBox.IdentityServer.Services
{
    public interface INotificationsService
    {
        Task SendResetPasswordNotificationAsync(SendResetPasswordNotificationCommand command);

        Task SendVerificationEmailAsync(SendVerificationEmailCommand command);
    }
    public class NotificationsService : INotificationsService
    {
        private readonly NotificationsServiceSettings _settings;

        private readonly ILogger<NotificationsService> _logger;

        private readonly Clients.CommunicationsClient _client;

        public NotificationsService(ILogger<NotificationsService> logger,
            Clients.CommunicationsClient client,
            IOptionsSnapshot<NotificationsServiceSettings> snapshot)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _client = client ?? throw new ArgumentNullException(nameof(client));
            _settings = snapshot?.Value ?? throw new ArgumentNullException(nameof(NotificationsServiceSettings));
        }

        public async Task SendResetPasswordNotificationAsync(SendResetPasswordNotificationCommand command)
        {
            if (command is null)
            {
                throw new ArgumentNullException(nameof(command));
            }

            _logger.LogOperationStarted("Send", "Reset Password Notification", command.Email);

            command.CheckNotEmpty(command.Email, SendResetPasswordNotificationCommand.EmailRequired);
            command.CheckNotEmpty(command.ResetToken, SendResetPasswordNotificationCommand.ResetTokenRequired);

            if (command.HasErrors)
            {
                _logger.LogOperationFailed("Send", "Reset Password Notification", string.Join(", ", command.Errors), command.Email);
                return;
            }

            try
            {
                var link = _settings.LinkTemplate.Replace("{token}", System.Web.HttpUtility.UrlEncode(command.ResetToken));

                var body = Models.Templates.ResetPasswordTemplate.Replace("{link}", link);

                var request = new Clients.SendEmailRequest
                {
                    Addressee = command.Email,
                    Subject = "DBox: Сброс пароля",
                    Body = body
                };

                await _client.SendEmailAsync(request);

                _logger.LogOperationCompleted("Send", "Reset Password Notification", command.Email);
            }
            catch (Exception e)
            {
                command.AddError(SendResetPasswordNotificationCommand.Internal);
                _logger.LogOperationFailed("Send", "Reset Password Notification", e, command.Email);
            }
        }

        public async Task SendVerificationEmailAsync(SendVerificationEmailCommand command)
        {
            if (command is null)
            {
                throw new ArgumentNullException(nameof(command));
            }

            _logger.LogOperationStarted("Send", "Verification Email", command.Recepient);

            command.CheckNotEmpty(command.Code, SendVerificationEmailCommand.CodeRequired);
            command.CheckNotEmpty(command.Recepient, SendVerificationEmailCommand.RecepientRequired);

            if (command.HasErrors)
            {
                _logger.LogOperationFailed("Send", "Verification Email", string.Join(", ", command.Errors), command.Recepient);
                return;
            }

            try
            {
                var message = _settings.ConfirmationMessageTemplate.Replace("{{code}}", command.Code);

                var request = new Clients.SendEmailRequest
                {
                    Addressee = command.Recepient,
                    Subject = "DBox: Ваш код подтверждения",
                    Body = message
                };

                await _client.SendEmailAsync(request);

                _logger.LogOperationCompleted("Send", "Verification Email", command.Recepient);
            }
            catch (Exception e)
            {
                command.AddError(SendVerificationEmailCommand.Internal);
                _logger.LogOperationFailed("Send", "Verification Email", e, command.Recepient);
            }
        }

    }

    #region models
    public class NotificationsServiceSettings
    {
        public const string SectionName = nameof(NotificationsServiceSettings);

        public string LinkTemplate { get; set; }
        public string ConfirmationMessageTemplate { get; set; }
    }

    public enum NotificationsServiceError
    {
        Internal = -2,
        External = -1,
        Unknown = 0,
        EmailRequired = 1,
        ResetTokenRequired = 2,
        PhoneRequired = 3,
        ConfirmationCodeRequired = 4
    }

    public abstract class NotificationsServiceCommand : ServiceCommand { }

    public class SendResetPasswordNotificationCommand : NotificationsServiceCommand
    {
        public static Error EmailRequired = Error.Required(nameof(Email));
        public static Error ResetTokenRequired = Error.Required(nameof(ResetToken));

        public string Email { get; set; }
        public string ResetToken { get; set; }
    }

    public class SendVerificationEmailCommand : NotificationsServiceCommand
    {
        public static Error RecepientRequired = Error.Required(nameof(Recepient));
        public static Error CodeRequired = Error.Required(nameof(Code));

        public string Recepient { get; set; }
        public string Code { get; set; }
    }

    #endregion
}
