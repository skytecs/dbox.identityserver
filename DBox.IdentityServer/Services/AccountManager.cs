﻿using AutoMapper;
using DBox.IdentityServer.Extensions;
using DBox.IdentityServer.Models;
using IdentityServer4.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBox.IdentityServer.Services
{
    public interface IAccountManager
    {
        Task CreateAccountAsync(CreateAccountCommand command);
        Task CreateUserAccountAsync(CreateUserAccountCommand command);
        Task CreateEmptyAccountAsync(CreateEmptyAccountCommand command);
        Task<CreateResetPasswordTokenResult> CreateResetPasswordTokenAsync(CreateResetPasswordTokenCommand command);
        Task ResetPasswordAsync(ResetPasswordCommand command);
        Task RemoveAccountAsync(RemoveAccountCommand command);
        Task UpdateAccountAsync(UpdateAccountCommand command);

        Task<List<AccountInfo>> GetAccountsAsync();
    }


    public class AccountManager : IAccountManager
    {
        private readonly ILogger<AccountManager> _logger;
        private readonly UserManager<DBoxUser> _userManager;
        private readonly IMapper _mapper;
        private readonly EmailTokenProvider<DBoxUser> _emailTokenProvider;
        private readonly AccountManagerSettings _settings;

        private readonly INotificationsService _notificationsService;

        public AccountManager(ILogger<AccountManager> logger,
            UserManager<DBoxUser> userManager,
            IMapper mapper,
            INotificationsService notificationsService,
            EmailTokenProvider<DBoxUser> emailTokenProvider,
            IOptionsSnapshot<AccountManagerSettings> snapshot)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _notificationsService = notificationsService ?? throw new ArgumentNullException(nameof(notificationsService));
            _emailTokenProvider = emailTokenProvider ?? throw new ArgumentNullException(nameof(emailTokenProvider));
            _settings = snapshot.Value ?? throw new ArgumentNullException(nameof(AccountManagerSettings));
        }

        public async Task CreateAccountAsync(CreateAccountCommand command)
        {
            if (command is null)
            {
                throw new ArgumentNullException(nameof(command));
            }

            _logger.LogOperationStarted("Create", "Account", command.Login);

            command.CheckNotEmpty(command.Login, CreateAccountCommand.LoginRequired);
            command.CheckNotEmpty(command.Password, CreateAccountCommand.PasswordRequired);

            if (command.HasErrors)
            {
                _logger.LogOperationFailed("Create", "Account", string.Join(", ", command.Errors), command.Login);
                return;
            }

            var user = new DBoxUser
            {
                Email = command.Login?.Trim(),
                UserName = command.Login?.Trim(),
                PhoneNumber = command.Phone?.Trim()
            };

            var result = await _userManager.CreateAsync(user, command.Password?.Trim());

            if (!result.Succeeded)
            {
                var errors = result.Errors;

                _logger.LogOperationFailed("Create", "Account", string.Join(", ", result.Errors.Select(x => x.Description)), command.Login);

                foreach (var error in result.Errors)
                {
                    command.AddError(error);
                }
                return;
            }

            _logger.LogOperationCompleted("Create", "Account", command.Login);
        }

        public async Task CreateUserAccountAsync(CreateUserAccountCommand command)
        {
            if (command is null)
            {
                throw new ArgumentNullException(nameof(command));
            }

            _logger.LogOperationStarted("Create", "User Account", command.Email);

            command.CheckNotEmpty(command.Email, CreateUserAccountCommand.EmailRequired);

            if (command.HasErrors)
            {
                _logger.LogOperationFailed("Create", "User Account", string.Join(", ", command.Errors), command.Email);
                return;

            }

            var email = _userManager.NormalizeName(command.Email);
            var user = await _userManager.Users.SingleOrDefaultAsync(x => x.Email == email)
                       ?? new DBoxUser
                       {
                           Email = command.Email,
                           SecurityStamp = new Secret("ED95809C-3885-45BD-ABDD-0F0F121BC94E").Value + command.Email.Sha256()
                       };

            var verifyToken = await _emailTokenProvider.GenerateAsync("verify_number", _userManager, user);

            _logger.LogOperationContinues("Create", "User Account", $"Genereted token {verifyToken}", command.Email);

            var sendEmailCommand = new SendVerificationEmailCommand
            {
                Recepient = command.Email,
                Code = verifyToken
            };
            
            try { 
                if (!_settings.AllowTestAccount || !_settings.TestAccountPhones.Contains(email))
                {
                    await _notificationsService.SendVerificationEmailAsync(sendEmailCommand);
                }
            } catch (Exception e) {
                _logger.LogError("Exception {Error} while sending command to notifications service", e.Message);
            }

            if (sendEmailCommand.HasErrors)
            {
                _logger.LogOperationFailed("Create", "User Account", string.Join(", ", command.Errors), command.Email);

                foreach (var error in sendEmailCommand.Errors)
                {
                    command.AddError(error);
                }
                return;
            }

            _logger.LogOperationCompleted("Create", "User Account", command.Email, $": account {user.Id} created");
        }

        public async Task CreateEmptyAccountAsync(CreateEmptyAccountCommand command)
        {
            if (command is null)
            {
                throw new ArgumentNullException(nameof(command));
            }

            _logger.LogOperationStarted("Create", "Empty account", command.Login);

            command.CheckNotEmpty(command.Login, CreateEmptyAccountCommand.LoginRequired);

            if (command.HasErrors)
            {
                _logger.LogOperationFailed("Create", "Empty account", string.Join(", ", command.Errors), command.Login);
                return;
            }

            var user = new DBoxUser
            {
                Email = command.Login?.Trim(),
                UserName = command.Login?.Trim()
            };

            var result = await _userManager.CreateAsync(user);

            if (!result.Succeeded)
            {
                var errors = result.Errors;

                _logger.LogOperationFailed("Create", "Empty account", string.Join(", ", result.Errors.Select(x => x.Description)), command.Login);

                foreach (var error in result.Errors)
                {
                    command.AddError(error);
                }
                return;
            }

            _logger.LogOperationCompleted("Create", "Empty account", command.Login, $": account {user.Id} created");
        }

        public async Task<CreateResetPasswordTokenResult> CreateResetPasswordTokenAsync(CreateResetPasswordTokenCommand command)
        {
            if (command is null)
            {
                throw new ArgumentNullException(nameof(command));
            }

            _logger.LogOperationStarted("Create", "Reset Password Token", $"{command.Email} {command.Phone}");

            command.CheckNotEmpty(command.Email, CreateResetPasswordTokenCommand.EmailRequired);

            if (command.HasErrors)
            {
                _logger.LogOperationFailed("Create", "Reset Password Token", string.Join(", ", command.Errors), $"{command.Email} {command.Phone}");
                return CreateResetPasswordTokenResult.Failed;
            }

            DBoxUser user = null;
            if (string.IsNullOrEmpty(command.Phone?.Trim()))
            {
                user = await _userManager.FindByEmailAsync(command.Email.Trim());
            }
            else
            {
                user = _userManager.Users.Where(x => x.PhoneNumber.Equals(command.Phone.Trim())).FirstOrDefault();
            }

            command.CheckNotNull(user, CreateResetPasswordTokenCommand.UserNotFound);

            if (command.HasErrors)
            {
                _logger.LogOperationFailed("Create", "Reset Password Token", string.Join(", ", command.Errors), $"{command.Email} {command.Phone}");
                return CreateResetPasswordTokenResult.Failed;
            }

            var token = await _userManager.GeneratePasswordResetTokenAsync(user);

            _logger.LogOperationCompleted("Create", "Reset Password Token", $"{command.Email} {command.Phone}", $". Token {token} generated");

            return new CreateResetPasswordTokenResult
            {
                Token = token
            };
        }

        public async Task<List<AccountInfo>> GetAccountsAsync()
        {
            return _mapper.Map<List<DBoxUser>, List<AccountInfo>>(_userManager.Users.ToList());
        }

        public async Task ResetPasswordAsync(ResetPasswordCommand command)
        {
            if (command is null)
            {
                throw new ArgumentNullException(nameof(command));
            }

            _logger.LogOperationStarted("Reset", "Password", command.Email);

            command.CheckNotEmpty(command.Email, ResetPasswordCommand.EmailRequired);
            command.CheckNotEmpty(command.NewPassword, ResetPasswordCommand.NewPasswordRequired);
            command.CheckNotEmpty(command.ResetToken, ResetPasswordCommand.ResetTokenRequired);


            if (command.HasErrors)
            {
                _logger.LogOperationFailed("Reset", "Password", string.Join(", ", command.Errors), command.Email);
                return;
            }

            var user = await _userManager.FindByEmailAsync(command.Email);

            command.CheckNotNull(user, ResetPasswordCommand.UserNotFound);

            if (command.HasErrors)
            {
                _logger.LogOperationFailed("Reset", "Password", string.Join(", ", command.Errors), command.Email);
                return;
            }

            var result = await _userManager.ResetPasswordAsync(user, command.ResetToken, command.NewPassword);

            if (!result.Succeeded)
            {
                var errors = result.Errors;

                _logger.LogOperationFailed("Reset", "Password", string.Join(", ", result.Errors.Select(x => x.Description)), command.Email);

                foreach (var error in result.Errors)
                {
                    command.AddError(error);
                }

                return;
            }

            _logger.LogOperationCompleted("Reset", "Password", command.Email);
        }

        public async Task RemoveAccountAsync(RemoveAccountCommand command)
        {
            if (command is null)
            {
                throw new ArgumentNullException(nameof(command));
            }

            _logger.LogOperationStarted("Remove", "Account", command.Login);

            command.CheckNotEmpty(command.Login, RemoveAccountCommand.LoginRequired);

            if (command.HasErrors)
            {
                _logger.LogOperationFailed("Remove", "Account", string.Join(", ", command.Errors), command.Login);
                return;
            }

            var user = await _userManager.FindByNameAsync(command.Login);

            command.CheckNotNull(user, RemoveAccountCommand.UserNotFound);

            if (command.HasErrors)
            {
                _logger.LogOperationFailed("Remove", "Account", string.Join(", ", command.Errors), command.Login);
                return;
            }

            var result = await _userManager.DeleteAsync(user);

            if (!result.Succeeded)
            {
                var errors = result.Errors;

                _logger.LogOperationFailed("Remove", "Account", string.Join(", ", result.Errors.Select(x => x.Description)), command.Login);

                foreach (var error in result.Errors)
                {
                    command.AddError(error);
                }

                return;
            }

            _logger.LogOperationCompleted("Remove", "Account", command.Login);
        }

        public async Task UpdateAccountAsync(UpdateAccountCommand command)
        {
            if (command is null)
            {
                throw new ArgumentNullException(nameof(command));
            }

            _logger.LogOperationStarted("Update", "Account", command.Login);

            command.CheckNotEmpty(command.Login, UpdateAccountCommand.LoginRequired);
            command.CheckEmailOrPhoneNotEmpty();

            if (command.HasErrors)
            {
                _logger.LogOperationFailed("Update", "Account", string.Join(", ", command.Errors), command.Login);
                return;
            }

            var user = await _userManager.FindByNameAsync(command.Login);

            command.CheckNotNull(user, UpdateAccountCommand.UserNotFound);

            if (command.HasErrors)
            {
                _logger.LogOperationFailed("Update", "Account", string.Join(", ", command.Errors), command.Login);
                return;
            }
            if (!string.IsNullOrWhiteSpace(command.NewEmail))
            {
                user.Email = command.NewEmail;
            }

            if (!string.IsNullOrWhiteSpace(command.NewPhone))
            {
                user.PhoneNumber = command.NewPhone;
            }

            var result = await _userManager.UpdateAsync(user);


            if (!result.Succeeded)
            {
                var errors = result.Errors;

                _logger.LogOperationFailed("Update", "Account", string.Join(", ", result.Errors.Select(x => x.Description)), command.Login);

                foreach (var error in result.Errors)
                {
                    command.AddError(error);
                }

                return;
            }

            _logger.LogOperationCompleted("Update", "Account", command.Login);
        }
    }

    #region Models
    public class AccountManagerSettings
    {
        public bool AllowTestAccount { get; set; }
        public List<string> TestAccountPhones { get; set; }
    }
    public abstract class AccountManagerCommand : ServiceCommand
    {
        public static Error EmailInUse = new Error("Email is in use");
        public static Error EmailInvalid = new Error("Email is invalid");
        public static Error PasswordRequiresNonAlphanumeric = new Error("Password requires non alphanumeric chars");
        public static Error PassportRequiresLowercase = new Error("Password requires lowercase chars");
        public static Error PassportRequiresUppercase = new Error("Password requires uppercase chars");
        public static Error PasswordRequiresDigits = new Error("Password requires digits");
        public static Error PasswordTooShort = new Error("Password is too short");

        public static Error UserNotFound = new Error("User was not found");

        public void AddError(IdentityError error)
        {
            if (error is null)
            {
                throw new ArgumentNullException(nameof(error));
            }

            var accountManagerError = error.Code switch
            {
                "DuplicateEmail" => AccountManagerCommand.EmailInUse,
                "DuplicateUserName" => AccountManagerCommand.EmailInUse,
                "LoginAlreadyAssociated" => AccountManagerCommand.EmailInUse,
                "InvalidEmail" => AccountManagerCommand.EmailInvalid,
                "PasswordRequiresNonAlphanumeric" => AccountManagerCommand.PasswordRequiresNonAlphanumeric,
                "PasswordRequiresLower" => AccountManagerCommand.PassportRequiresLowercase,
                "PasswordRequiresUpper" => AccountManagerCommand.PassportRequiresUppercase,
                "PasswordTooShort" => AccountManagerCommand.PasswordTooShort,
                "PasswordRequiresDigit" => AccountManagerCommand.PasswordRequiresDigits,

                _ => AccountManagerCommand.Internal
            };

            AddError(accountManagerError);
        }
    }

    public class CreateAccountCommand : AccountManagerCommand
    {
        public static Error LoginRequired = Error.Required(nameof(Login));
        public static Error PasswordRequired = Error.Required(nameof(Password));

        public string Login { get; set; }
        public string Phone { get; set; }
        public string Password { get; set; }
    }

    public class CreateUserAccountCommand : AccountManagerCommand
    {
        public static Error EmailRequired = Error.Required(nameof(Email));
        public string Email { get; set; }
    }

    public class CreateEmptyAccountCommand : AccountManagerCommand
    {
        public static Error LoginRequired = Error.Required(nameof(Login));

        public string Login { get; set; }
    }

    public class CreateResetPasswordTokenCommand : AccountManagerCommand
    {
        public static Error EmailRequired = Error.Required(nameof(Email));

        public string Email { get; set; }
        public string Phone { get; set; }
    }

    public class CreateResetPasswordTokenResult
    {
        public static CreateResetPasswordTokenResult Failed = new CreateResetPasswordTokenResult();

        public string Token { get; set; }
    }

    public class ResetPasswordCommand : AccountManagerCommand
    {
        public static Error EmailRequired = Error.Required(nameof(Email));
        public static Error NewPasswordRequired = Error.Required(nameof(NewPassword));
        public static Error ResetTokenRequired = Error.Required(nameof(ResetTokenRequired));

        public string Email { get; set; }
        public string NewPassword { get; set; }
        public string ResetToken { get; set; }

    }

    public class RemoveAccountCommand : AccountManagerCommand
    {
        public static Error LoginRequired = Error.Required(nameof(Login));

        public string Login { get; set; }
    }

    public class UpdateAccountCommand : AccountManagerCommand
    {
        public static Error LoginRequired = Error.Required(nameof(Login));
        public static Error PhoneOrEmailRequired = new Error("Phone or Email required");

        public void CheckEmailOrPhoneNotEmpty()
        {
            if (string.IsNullOrWhiteSpace(NewPhone) && string.IsNullOrWhiteSpace(NewEmail))
            {
                AddError(UpdateAccountCommand.PhoneOrEmailRequired);
            }
        }

        public string Login { get; set; }
        public string NewPhone { get; set; }
        public string NewEmail { get; set; }

        public string UserId { get; set; }
    }

    public class AccountInfo
    {
        public string Email { get; set; }
    }
    #endregion
}
