﻿using Consul;
using DBox.Extensions;
using DBox.IdentityServer.Extensions;
using DBox.IdentityServer.Models;
using DBox.IdentityServer.Validation;
using IdentityServer4.EntityFramework.DbContexts;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;
using Serilog;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;

namespace DBox.IdentityServer
{
    public class Startup
    {
        public IWebHostEnvironment Environment { get; }
        public IConfiguration Configuration { get; }

        public Startup(IWebHostEnvironment environment, IConfiguration configuration)
        {
            Environment = environment;
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<Services.NotificationsServiceSettings>(Configuration.GetSection(Services.NotificationsServiceSettings.SectionName));
            services.Configure<Services.AccountManagerSettings>(Configuration.GetSection(nameof(Services.AccountManagerSettings)));

            services.AddAutoMapper(GetType());

            services.AddSingleton<IConsulClient, ConsulClient>();

            services.AddDbContext<DBoxIdentityContext>(options =>
            {
                options.UseNpgsql(Configuration.GetConnectionString("DefaultConnection"));
            });

            services.AddIdentityCore<DBoxUser>(options =>
            {
                options.SignIn.RequireConfirmedAccount = false;
            })
                .AddRoles<DBoxRole>()
                .AddEntityFrameworkStores<DBoxIdentityContext>()
                .AddDefaultTokenProviders();

            var builder = services.AddIdentityServer(options =>
            {
                options.Events.RaiseErrorEvents = true;
                options.Events.RaiseInformationEvents = true;
                options.Events.RaiseFailureEvents = true;
                options.Events.RaiseSuccessEvents = true;

                // see https://identityserver4.readthedocs.io/en/latest/topics/resources.html
                options.EmitStaticAudienceClaim = false;

            }).AddOperationalStore(options =>
            {
                options.ConfigureDbContext = builder => builder
                .UseNpgsql(Configuration.GetConnectionString("DefaultConnection"), sql => sql.MigrationsAssembly(GetType().Assembly.FullName));

                options.EnableTokenCleanup = true;
                options.TokenCleanupInterval = 36000;

            }).AddExtensionGrantValidator<EmailTokenGrantValidator>()
            .AddAspNetIdentity<DBoxUser>()
            .AddInMemoryApiScopes(Config.ApiScopes)
            .AddInMemoryApiResources(Config.ApiResources)
            .AddInMemoryClients(Config.Clients);

            services.AddHttpClient<Clients.CommunicationsClient>((provider, client) => client.BaseAddress = provider.DiscoverServiceUrl("CommunicationsServiceName"));

            services.AddScoped<IUserValidator<DBoxUser>, UserValidator<DBoxUser>>();
            services.AddScoped<IPasswordValidator<DBoxUser>, PasswordValidator<DBoxUser>>();
            services.AddScoped<IPasswordHasher<DBoxUser>, PasswordHasher<DBoxUser>>();
            services.AddScoped<ILookupNormalizer, UpperInvariantLookupNormalizer>();
            services.AddScoped<IRoleValidator<DBoxRole>, RoleValidator<DBoxRole>>();
            // No interface for the error describer so we can add errors without rev'ing the interface
            services.AddScoped<IdentityErrorDescriber>();
            services.AddScoped<ISecurityStampValidator, SecurityStampValidator<DBoxUser>>();
            services.AddScoped<ITwoFactorSecurityStampValidator, TwoFactorSecurityStampValidator<DBoxUser>>();
            services.AddScoped<IUserClaimsPrincipalFactory<DBoxUser>, UserClaimsPrincipalFactory<DBoxUser, DBoxRole>>();
            services.AddScoped<UserManager<DBoxUser>>();
            services.AddScoped<SignInManager<DBoxUser>>();
            services.AddScoped<RoleManager<DBoxRole>>();

            services.Configure<DataProtectionTokenProviderOptions>(opt => opt.TokenLifespan = TimeSpan.FromHours(8));

            // not recommended for production - you need to store your key material somewhere secure
            builder.AddDeveloperSigningCredential();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.Authority = Configuration.GetValue("IdentityServerAuthority", "");
                    options.Audience = "DBoxAccountsService";
                    options.RequireHttpsMetadata = false;

                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateLifetime = true,
                        ValidateAudience = true,
                        ValidateIssuer = false
                    };
                }).AddApplicationCookie();

            services.AddControllers();
            services.AddScoped<Services.IAccountManager, Services.AccountManager>();
            services.AddScoped<Services.INotificationsService, Services.NotificationsService>();
            services.AddTransient<IConfigureOptions<SwaggerGenOptions>, ConfigureSwaggerOptions>().AddSwaggerGen();
        }

        public void Configure(IApplicationBuilder app, ILogger<Startup> logger, IHost host, IHostApplicationLifetime lifetime)
        {
            if (Environment.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                IdentityModelEventSource.ShowPII = true;
            }

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.RoutePrefix = "swagger";
                c.SwaggerEndpoint("v1/swagger.json", "DBox IdentityServer API V1");
            });

            app.UseSerilogRequestLogging();
            app.UseRouting();
            app.UseIdentityServer();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
            });

            app.RegisterWithConsul(lifetime);

            try
            {
                using var scope = app.ApplicationServices.CreateScope();
                using var dataContext = scope.ServiceProvider.GetRequiredService<DBoxIdentityContext>();

                dataContext.Database.Migrate();
            }
            catch (Exception e)
            {
                logger.LogCritical(e, "Failed to migrate DBoxIdentityContext");

                host.StopAsync().Wait();
            }

            try
            {
                using var scope = app.ApplicationServices.CreateScope();
                using var dataContext = scope.ServiceProvider.GetRequiredService<PersistedGrantDbContext>();

                dataContext.Database.Migrate();
            }
            catch (Exception e)
            {
                logger.LogCritical(e, "Failed to migrate PersistedGrantDbContext");

                host.StopAsync().Wait();
            }
        }
    }
}