﻿using System.Collections.Generic;

namespace DBox.IdentityServer.Controllers
{
    public class ErrorsResponse
    {
        public List<Error> Errors { get; set; } = new List<Error>();

        public ErrorsResponse() { }

        public ErrorsResponse(Error error)
        {
            Errors.Add(error);
        }
    }
}
