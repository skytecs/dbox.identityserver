﻿using System.Collections.Generic;

namespace DBox.IdentityServer.Controllers
{
    public class Errors
    {
        public static Error Internal = new Error(0, "Internal error");
        public static Error UserNotFound = new Error(1, "User not found");
        public static Error DuplicateEmail = new Error(2, "User with given email exists");
        public static Error EmailInvalid = new Error(3, "Email is invalid");
        public static Error PasswordRequiresNonAlphanumeric = new Error(4, "Password must content non-alphanumeric chars");
        public static Error PasswordRequiresLower = new Error(5, "Password required lowercase chars");
        public static Error PasswordRequiresUpper = new Error(6, "Password required uppercase chars");
        public static Error PasswordRequiresDigit = new Error(7, "Password requires digits");
        public static Error PasswordTooShort = new Error(8, "Password too short");

        public static Error LoginRequired = new Error(9, "Login required");

        public static HashSet<Error> GetErrors() => new HashSet<Error>
        {
            Internal,
            UserNotFound,
            DuplicateEmail,
            EmailInvalid,
            PasswordRequiresNonAlphanumeric,
            PasswordRequiresLower,
            PasswordRequiresUpper,
            PasswordRequiresDigit,
            PasswordTooShort,
            LoginRequired
        };
    }

    public class Error
    {
        public long Code { get; set; }
        public string Description { get; set; }

        public Error(long code, string description)
        {
            Code = code;
            Description = description;
        }
    }
}
