﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace DBox.IdentityServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ErrorsController : ControllerBase
    {
        [HttpGet(Name = nameof(ErrorsAsync))]
        [ProducesResponseType(typeof(List<Error>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> ErrorsAsync()
        {
            return Ok(Errors.GetErrors());
        }


    }
}
