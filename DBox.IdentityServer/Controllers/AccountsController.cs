﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Threading.Tasks;

namespace DBox.IdentityServer.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    [Authorize]
    public class AccountsController : ControllerBase
    {
        private readonly ILogger<AccountsController> _logger;
        private readonly Services.IAccountManager _accountManager;
        private readonly IMapper _mapper;

        private BadRequestObjectResult BadRequest(Services.ServiceCommand command)
        {
            var response = new ErrorsResponse();

            foreach (var error in command.Errors)
            {
                if (error == Services.CreateEmptyAccountCommand.LoginRequired)
                {
                    response.Errors.Add(Errors.LoginRequired);
                    continue;
                }
                if (error == Services.CreateEmptyAccountCommand.EmailInUse)
                {
                    response.Errors.Add(Errors.DuplicateEmail);
                    continue;
                }
                if (error == Services.CreateEmptyAccountCommand.EmailInvalid)
                {
                    response.Errors.Add(Errors.EmailInvalid);
                    continue;
                }
                if (error == Services.AccountManagerCommand.UserNotFound)
                {
                    response.Errors.Add(Errors.UserNotFound);
                    continue;
                }
                if (error == Services.AccountManagerCommand.PasswordRequiresNonAlphanumeric)
                {
                    response.Errors.Add(Errors.PasswordRequiresNonAlphanumeric);
                    continue;
                }
                if (error == Services.AccountManagerCommand.PassportRequiresLowercase)
                {
                    response.Errors.Add(Errors.PasswordRequiresLower);
                    continue;
                }
                if (error == Services.AccountManagerCommand.PassportRequiresUppercase)
                {
                    response.Errors.Add(Errors.PasswordRequiresUpper);
                    continue;
                }
                if (error == Services.AccountManagerCommand.PasswordRequiresDigits)
                {
                    response.Errors.Add(Errors.PasswordRequiresDigit);
                    continue;
                }
                if (error == Services.AccountManagerCommand.PasswordTooShort)
                {
                    response.Errors.Add(Errors.PasswordTooShort);
                    continue;
                }

                response.Errors.Add(Errors.Internal);
            }

            return BadRequest(response);
        }

        public AccountsController(ILogger<AccountsController> logger,
            Services.IAccountManager accountManager,
            IMapper mapper)
        {
            _logger = logger ?? throw new System.ArgumentNullException(nameof(logger));
            _accountManager = accountManager ?? throw new System.ArgumentNullException(nameof(accountManager));
            _mapper = mapper ?? throw new System.ArgumentNullException(nameof(mapper));
        }

        [HttpGet(Name = nameof(GetAccountsAsync))]
        [ProducesResponseType(typeof(List<Account>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetAccountsAsync()
        {
            var accounts = await _accountManager.GetAccountsAsync();
            var response = _mapper.Map<List<Services.AccountInfo>, List<Account>>(accounts);
            return Ok(response);
        }

        [HttpPost("empty", Name = nameof(CreateEmptyAccountAsync))]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        [ProducesResponseType(typeof(ErrorsResponse), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> CreateEmptyAccountAsync([FromBody] CreateEmptyAccountRequest request)
        {
            var command = _mapper.Map<Services.CreateEmptyAccountCommand>(request);
            await _accountManager.CreateEmptyAccountAsync(command);

            if (!command.HasErrors)
            {
                return NoContent();
            }

            return BadRequest(command);
        }

        [HttpPost(Name = "CreatAccount")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        [ProducesResponseType(typeof(ErrorsResponse), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> CreateAccountAsync([FromBody] CreateAccountRequest request)
        {
            var command = _mapper.Map<Services.CreateAccountCommand>(request);

            await _accountManager.CreateAccountAsync(command);

            if (!command.HasErrors)
            {
                return NoContent();
            }

            return BadRequest(command);
        }

        [HttpPost("email/requestValidation", Name = "RequestAccountEmailValidation")]
        [AllowAnonymous]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        [ProducesResponseType(typeof(ErrorsResponse), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> RequestAccountEmailValidationAsync([FromBody] CreateEmailAccountRequest request)
        {
            var command = _mapper.Map<Services.CreateUserAccountCommand>(request);

            await _accountManager.CreateUserAccountAsync(command);

            if (!command.HasErrors)
            {
                return NoContent();
            }

            return BadRequest(command);
        }


        [HttpPost("forgot", Name = "ForgotPassword")]
        [ProducesResponseType(typeof(ForgotPasswordResponse), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsResponse), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> ForgotPasswordAsync([FromBody] ForgotPasswordRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var command = _mapper.Map<Services.CreateResetPasswordTokenCommand>(request);

            var result = await _accountManager.CreateResetPasswordTokenAsync(command);

            if (!command.HasErrors)
            {
                return Ok(result);
            }

            return BadRequest(command);
        }

        [HttpPost("reset", Name = "ResetPassword")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        [ProducesResponseType(typeof(ErrorsResponse), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> ResetPasswordAsync([FromBody] ResetPasswordRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var command = _mapper.Map<Services.ResetPasswordCommand>(request);

            await _accountManager.ResetPasswordAsync(command);

            if (!command.HasErrors)
            {
                return NoContent();
            }

            return BadRequest(command);

        }

        [HttpDelete(Name = nameof(RemoveAsync))]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        [ProducesResponseType(typeof(ErrorsResponse), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> RemoveAsync([FromQuery] RemoveAccountRequest request)
        {
            var command = _mapper.Map<Services.RemoveAccountCommand>(request);

            await _accountManager.RemoveAccountAsync(command);

            if (!command.HasErrors)
            {
                return NoContent();
            }

            return BadRequest(command);
        }

        [HttpPut(Name = "UpdateAccountData")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        [ProducesResponseType(typeof(ErrorsResponse), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UpdateAccountDataAsync([FromBody] UpdateAccountRequest request)
        {
            var command = _mapper.Map<Services.UpdateAccountCommand>(request);

            await _accountManager.UpdateAccountAsync(command);

            if (!command.HasErrors)
            {
                return NoContent();
            }

            return BadRequest(command);
        }
    }

    #region models
    public class CreateAccountRequest
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }

    public class CreateEmailAccountRequest
    {
        public string Email { get; set; }
    }
    public class CreateEmptyAccountRequest
    {
        public string Login { get; set; }
    }

    public class ForgotPasswordRequest
    {
        [Required]
        public string Email { get; set; }
    }

    public class ForgotPasswordResponse
    {
        public string Token { get; set; }
    }

    public class ResetPasswordRequest
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public string NewPassword { get; set; }
        [Required]
        public string ResetToken { get; set; }
    }

    public class RemoveAccountRequest
    {
        public string Login { get; set; }
    }
    public class Account
    {
        public string Email { get; set; }
    }

    public class UpdateAccountRequest
    {
        public string Login { get; set; }
        public string NewPhone { get; set; }
        public string NewEmail { get; set; }
    }
    #endregion
}
