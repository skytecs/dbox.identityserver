﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DBox.IdentityServer.Models
{
    public class DBoxIdentityContext : IdentityDbContext<DBoxUser, DBoxRole, long>
    {
        public DBoxIdentityContext(DbContextOptions<DBoxIdentityContext> options) : base(options) { }

    }

    public class DBoxUser : IdentityUser<long> { }
    public class DBoxRole : IdentityRole<long> { }
}
