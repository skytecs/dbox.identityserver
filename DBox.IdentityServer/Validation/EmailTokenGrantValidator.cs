﻿using DBox.IdentityServer.Models;
using DBox.IdentityServer.Services;
using IdentityModel;
using IdentityServer4.Models;
using IdentityServer4.Validation;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;

namespace DBox.IdentityServer.Validation
{
    public class EmailTokenGrantValidator : IExtensionGrantValidator
    {
        private readonly EmailTokenProvider<DBoxUser> _emailTokenProvider;
        private readonly UserManager<DBoxUser> _userManager;
        private readonly SignInManager<DBoxUser> _signInManager;
        private readonly ILogger<EmailTokenGrantValidator> _logger;
        private readonly AccountManagerSettings _settings;

        public EmailTokenGrantValidator(
            EmailTokenProvider<DBoxUser> emailTokenProvider,
            UserManager<DBoxUser> userManager,
            SignInManager<DBoxUser> signInManager,
            ILogger<EmailTokenGrantValidator> logger,
            IOptionsSnapshot<AccountManagerSettings> snapshot)
        {
            _emailTokenProvider = emailTokenProvider ?? throw new System.ArgumentNullException(nameof(emailTokenProvider));
            _userManager = userManager ?? throw new System.ArgumentNullException(nameof(userManager));
            _signInManager = signInManager ?? throw new System.ArgumentNullException(nameof(signInManager));
            _logger = logger ?? throw new System.ArgumentNullException(nameof(logger));
            _settings = snapshot.Value ?? throw new System.ArgumentNullException(nameof(AccountManagerSettings));
        }

        public async Task ValidateAsync(ExtensionGrantValidationContext context)
        {
            var createUser = false;
            var raw = context.Request.Raw;
            var credential = raw.Get(OidcConstants.TokenRequest.GrantType);
            if (credential == null || credential != GrantTypeConst.EmailToken)
            {
                context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant,
                    "invalid verify_email_number_token credential");
                return;
            }

            var email = raw.Get(TokenRequest.Email);
            var verificationToken = raw.Get(TokenRequest.Token);

            var user = await _userManager.Users.SingleOrDefaultAsync(x =>
                x.Email == _userManager.NormalizeName(email));

            if (user == null)
            {
                user = await _userManager.Users.SingleOrDefaultAsync(x => x.UserName == email);
                if (user != null)
                {
                    user.Email = email;
                    user.SecurityStamp = new Secret("ED95809C-3885-45BD-ABDD-0F0F121BC94E").Value + email.Sha256();
                    await _userManager.UpdateAsync(user);
                }
            }

            if (user == null)
            {
                user = new DBoxUser
                {
                    UserName = email,
                    Email = email,
                    SecurityStamp = new Secret("ED95809C-3885-45BD-ABDD-0F0F121BC94E").Value + email.Sha256()
                };
                createUser = true;
            }

            var result = await _emailTokenProvider.ValidateAsync("verify_number", verificationToken, _userManager, user);

            if (_settings.AllowTestAccount && _settings.TestAccountPhones.Contains(email) && verificationToken.Equals("000000"))
            {
                result = true;
            }

            if (!result)
            {
                _logger.LogInformation("Authentication failed for token: {token}, reason: invalid token",
                    verificationToken);
                return;
            }

            if (!createUser && !user.EmailConfirmed)
            {
                user.EmailConfirmed = true;
                await _userManager.UpdateAsync(user);
            }

            if (createUser)
            {
                user.EmailConfirmed = true;
                var resultCreation = await _userManager.CreateAsync(user);
                if (resultCreation != IdentityResult.Success)
                {
                    _logger.LogInformation("User creation failed: {username}, reason: invalid user", email);
                    return;
                }
            }

            _logger.LogInformation("Credentials validated for username: {email}", email);
            await _signInManager.SignInAsync(user, true);
            context.Result = new GrantValidationResult(user.Id.ToString(), OidcConstants.AuthenticationMethods.OneTimePassword);
        }

        public string GrantType => GrantTypeConst.EmailToken;

        public struct TokenRequest
        {
            public const string Email = "email";
            public const string Token = "verification_token";
        }

        public struct GrantTypeConst
        {
            public const string EmailToken = "email_token";
        }
    }
}
